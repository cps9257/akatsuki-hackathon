﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    public GameObject[] natureObj;
    public GameObject[] harmObj;
    public GameObject  spawnArea;
    public Transform spawnVineL, spawnVineR;
    
    //public float timeInterval;
    public float intervalMin, intervalMax;
    public int countMinHarm, countMaxHarm;
    public int countMinNature, countMaxNature;
    private Vector3 scale;
    private float time;
    //private GameObject leaveArea, spawnArea;
	// Use this for initialization
    void Awake()
    {
        
    }
	void Start () {
        scale = spawnArea.transform.lossyScale;
        float timeInterval = Random.Range(intervalMin, intervalMax);
        time = timeInterval;
	}
	
	// Update is called once per frame
	void Update () {
        time -= Time.deltaTime;
        //Debug.Log(time);
        if (time<=0.0f)
        {
            float timeInterval = Random.Range(intervalMin, intervalMax);
            time = timeInterval;
            int countHarm=Random.Range(countMinHarm,countMaxHarm);
            int countNature = Random.Range(countMinNature, countMaxNature);
            //Debug.Log("count"+count);
            SpawnObjects(countHarm, countNature);
        }
	}
    void SpawnObjects(int countHarm,int countNature)
    {
        Vector3 midPoint = spawnArea.transform.position;
        
        Vector3 leftTop=midPoint+-scale/2;
        Vector3 righBot=midPoint+scale/2;
        while (countHarm > 0)
        {
            //Random position
            float rdPosX = Random.Range(leftTop.x, righBot.x);
            float rdPosY = Random.Range(leftTop.y, righBot.y);
            Vector3 rdPos=new Vector3(rdPosX,rdPosY,0.0f);
            //Random kind
            int kindIndex = Random.Range(0, harmObj.Length);
            //Debug.Log(kindIndex);
            Instantiate(harmObj[kindIndex], rdPos, Quaternion.identity);
            countHarm--;
        }
        while (countNature > 0)
        {
            //Random position
            float rdPosX = Random.Range(leftTop.x, righBot.x);
            float rdPosY = Random.Range(leftTop.y, righBot.y);
            Vector3 rdPos = new Vector3(rdPosX, rdPosY, 0.0f);
            //Random kind
            int kindIndex = Random.Range(0, natureObj.Length);
            
            //Debug.Log(kindIndex);
            if (kindIndex>0)
            {
                if (kindIndex==1)
                    Instantiate(natureObj[kindIndex],spawnVineL.position, natureObj[kindIndex].transform.rotation);
                else if (kindIndex == 2)
                    Instantiate(natureObj[kindIndex],spawnVineR.position, natureObj[kindIndex].transform.rotation);
            }
            else
                Instantiate(natureObj[kindIndex], rdPos, Quaternion.identity);
            countNature--;
        }

    }
}
