﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Counter : MonoBehaviour {
    public int[] harmObjectCount;
    public float distanceSoFar;
    public float goalDistance;
    private float startY;
    private GameObject player;
    public int type = -1;
    public bool isFinish = false;
	// Use this for initialization
	void Start () {
        player=GameObject.FindGameObjectWithTag("Player");
        startY=player.transform.position.y;
        //distanceSoFar = player.transform.position.y;
		for(int i=0;i<harmObjectCount.Length;i++)
        {
            harmObjectCount[i] = 0;
        }
	}
	
	// Update is called once per frame
	void Update () {
        distanceSoFar = player.transform.position.y - startY;
        if (distanceSoFar>goalDistance && !isFinish)
        {

            isFinish = true;
            int maxNum = Mathf.Max(harmObjectCount);
            for (int i = 0; i < harmObjectCount.Length; i++)
            {
                if (maxNum == harmObjectCount[i])
                    type=maxNum;
            }
            GameObject.FindGameObjectWithTag("GameController").GetComponent<Spawner>().enabled = false;
            Invoke("Burst",7.0f);

            Debug.Log("type" + type);
            //into final result, calculate max harm object count, stop spawning
            
        }
	}
    public void Burst()
    {
        Camera.main.GetComponent<CameraController>().enabled = false;
        GameObject.FindGameObjectWithTag("Background").GetComponent<PlayerFollower>().enabled = false;
    }
    public void addNum(int index)
    {
        harmObjectCount[index-1] += 1;

    }
}
