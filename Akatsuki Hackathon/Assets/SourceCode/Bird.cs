﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : NatureObject{
    public float speed;
    public float RotationSpeed;
    private GameObject player;
    
    public enum State
    {
        Idle,
        Wander,
        Seek,
        Flee,
        Arrival,
        Separate
    }
    public State state;
    private Quaternion _lookRotation;
    private Vector3 _direction;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        movement();
	}
    void movement()
    {
        Vector3 currentpos = gameObject.transform.position;
        Vector3 targetpos = player.transform.position;
        gameObject.transform.position = Seek(currentpos, targetpos, speed);
        
        

    }
    public virtual Vector3 Seek(Vector3 currentPos, Vector3 targetPos, float speed)
    {
        Vector3 desired_velocity = (targetPos - currentPos).normalized * speed;
        //float velocityX=Mathf.Clamp(desired_velocity.x,0,)
        desired_velocity.y = 0.0f;
        //rotate toward player
        Vector3 _direction = (targetPos - currentPos).normalized;
        //create the rotation we need to be in to look at the target
        _lookRotation = Quaternion.LookRotation(_direction);
        //rotate us over time according to speed until we are in the required rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * RotationSpeed);
        transform.eulerAngles = new Vector3(0, 0, -transform.eulerAngles.z);
        
        return currentPos + desired_velocity * Time.deltaTime;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "LeaveArea")
            Destroy(gameObject);
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.SendMessage("Damaged", transform);
        }
    }
}
