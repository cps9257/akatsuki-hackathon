﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jelly : MonoBehaviour {
    private GameObject player;
    public float speed;
    public enum State
    {
        Idle,
        Wander,
        Seek,
        Flee,
        Arrival,
        Separate
    }
    public State state;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        movement();
    }
    void movement()
    {
        Vector3 currentpos = gameObject.transform.position;
        Vector3 targetpos = player.transform.position;
        gameObject.transform.position = Seek(currentpos, targetpos, speed);


    }
    public virtual Vector3 Seek(Vector3 currentPos, Vector3 targetPos, float speed)
    {
        Vector3 desired_velocity = (targetPos - currentPos).normalized * speed;
        //float velocityX=Mathf.Clamp(desired_velocity.x,0,)
        desired_velocity.y = 0.0f;
        return currentPos + desired_velocity * Time.deltaTime;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "LeaveArea")
            Destroy(gameObject);
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.SendMessage("Damaged", transform);
        }
    }
}
