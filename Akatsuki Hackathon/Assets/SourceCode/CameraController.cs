﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    private GameObject player;
    private float currentY;
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        currentY = transform.position.y-player.transform.position.y;
        
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position
        transform.position = new Vector3(transform.position.x, player.transform.position.y + currentY, transform.position.z);
    }
}
