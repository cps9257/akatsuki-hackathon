﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarmObject :PooledObject {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float rotateSpeed = Random.Range(-60.0f, 60.0f);
        transform.Rotate(new Vector3(0, 0, rotateSpeed) * Time.deltaTime);
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "LeaveArea")
        {
            //Add to object pool
            Destroy(gameObject);

            //ReturnToPool();
        }
        if (other.gameObject.tag == "Outsider" || other.gameObject.tag == "NatureObject" || other.gameObject.tag == "Plastic" || other.gameObject.tag == "SkyLight" || other.gameObject.tag == "Poison")
        {
            Destroy(gameObject);
            //gameObject.SetActive(false);
        }
        
    }
}
