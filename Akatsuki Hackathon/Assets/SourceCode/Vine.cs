﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vine : MonoBehaviour {
    public float speed;
    private Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity =new Vector2(0.0f, -speed);
        //gameObject.transform.position += new Vector3(0, Time.deltaTime * speed, 0);
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "LeaveArea")
            Destroy(gameObject);
        if (other.gameObject.tag=="Player")
        {

            other.gameObject.SendMessage("Damaged", transform);
        }
    }
}
