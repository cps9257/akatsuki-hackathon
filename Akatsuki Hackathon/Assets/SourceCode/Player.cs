﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {
    public GameObject counter;
    public float moveSpeed;
    public float fireForce;
    public float recoveryTime;
    private Rigidbody2D rb;
    private bool invincible;
    // Use this for initialization
	void Awake()
    {

    }
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        takeOff();
        invincible = false;
	}
	
	// Update is called once per frame
	void Update () {
        
        move();
	}
    void move()
    {
        //get the input from gyroscope
        float movement_V=Input.GetAxis("Horizontal");
        
        //Debug.Log(movement_V);
        /*
        if (Mathf.Abs(movement_V) < rotateThreshold)
        {
            gameObject.transform.localPosition +=new Vector3(movement_V*moveSpeed*Time.deltaTime, 0, 0);
                //new Vector3(movement_V*moveSpeed*Time.deltaTime, 0, 0);
        }
        */
        if (Mathf.Abs(movement_V)>=0.0)
            gameObject.transform.localPosition += new Vector3(movement_V * moveSpeed * Time.deltaTime, 0, 0);

    }
    void takeOff()
    {
        /*
        if(Input.GetKeyDown(KeyCode.Space))
        {

            Mathf.Clamp(fireForce,minFireForce, maxFireForce);
            
            //transform.position=Mathf.Lerp(currentPos.y, currentPos.y + fireForce, 2.0f);
            Rb.AddForce(new Vector2(0.0f, fireForce), ForceMode2D.Impulse);
            //Rb.gravityScale = 1.0f;
            //Debug.Log("Press space");
        }
        */
        rb.velocity = new Vector2(0.0f, fireForce);
        //Rb.AddForce(new Vector2(0.0f, fireForce), ForceMode2D.Impulse);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Plastic")
        {
            Destroy(other.gameObject);
            counter.SendMessage("addNum", 1);
            //other.gameObject.SetActive(false);
        }
        else if (other.gameObject.tag == "SkyLight")
        {
            Destroy(other.gameObject);
            counter.SendMessage("addNum", 2);
            //other.gameObject.SetActive(false);
        }
        else if (other.gameObject.tag == "Poison")
        {
            Destroy(other.gameObject);
            counter.SendMessage("addNum", 3);
            //other.gameObject.SetActive(false);
        }

            //Add to object pool
            //ReturnToPool();
    }
    public void Damaged(Transform damageSource)
    {
        if (!invincible)
        {
            StartCoroutine(FlashSprite(damageSource));
            //tno.Send("TriggerDamage", Target.Others);
        }
    }
    protected void TriggerDamage()
    {
        //anim.SetTrigger("TriggerDamage");
    }
    IEnumerator FlashSprite(Transform damageSource)
    {
        invincible = true;

        InvokeRepeating("Flicker", 0.0f, 0.2f);
        

        //gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        //rb.velocity = new Vector2((transform.position.x - damageSource.position.x) * 100, rb.velocity.y);

        //delayspecified amount
        yield return new WaitForSeconds(recoveryTime);
        //health--;
        CancelInvoke();
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        //gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        invincible = false;
        /*
        if (health == 0)
            HeroHasDied();
        */
    }
    void Flicker()
    {
        int flicker = Random.Range(0, 100);
        if (flicker < 50)
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
        else
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
    }
}
